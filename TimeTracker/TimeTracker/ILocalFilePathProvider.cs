﻿namespace TimeTracker
{
    public interface ILocalFilePathProvider
    {
        string GetLocalPathToFile(string fileName);
    }
}