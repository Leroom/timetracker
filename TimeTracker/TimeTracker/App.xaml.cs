﻿using Autofac;
using Autofac.Core;
using TimeTracker.Pages.TrackingsList;
using Xamarin.Forms;

namespace TimeTracker
{
    public partial class App : Application
    {
        private static TrackingsDatabase _database;
        public static TrackingsDatabase Database => GetDatabase();

        private static TrackingsDatabase GetDatabase()
        {
            if (_database == null)
            {
                _database = _container.Resolve<TrackingsDatabase>();
            }

            return _database;
        }

        private static IContainer _container;

        public App(IModule[] platformSpecificModules)
        {
            PrepareContainer(platformSpecificModules);

            InitializeComponent();
            MainPage = new NavigationPage(new TrackingsListPage());
        }

        private static void PrepareContainer(IModule[] platformSpecificModules)
        {
            var containerBuilder = new Autofac.ContainerBuilder();

            RegisterPlatformSpecificModules(platformSpecificModules, containerBuilder);

            containerBuilder.RegisterType<TrackingsDatabase>().SingleInstance();

            _container = containerBuilder.Build();
        }

        private static void RegisterPlatformSpecificModules(IModule[] platformSpecificModules, ContainerBuilder containerBuilder)
        {
            foreach (var platformSpecificModule in platformSpecificModules)
            {
                containerBuilder.RegisterModule(platformSpecificModule);
            }
        }

        protected override void OnStart()
        {

            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
