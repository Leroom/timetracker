﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace TimeTracker.Pages.Converters
{
    public class DurationToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var castedValue = (TimeSpan) value;
            return $"{Math.Floor(castedValue.TotalHours)}:{TurnIntoTwoDigits(castedValue.Minutes)}:{TurnIntoTwoDigits(castedValue.Seconds)}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static string TurnIntoTwoDigits(int number)
        {
            return number >= 0 && number <= 9 ? $"0{number}" : number.ToString();
        }
    }
}