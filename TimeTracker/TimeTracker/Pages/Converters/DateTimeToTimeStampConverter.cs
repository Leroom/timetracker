﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace TimeTracker.Pages.Converters
{
    public class DateTimeToTimeStampConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var castedValue = (DateTime) value;
            return castedValue.TimeOfDay;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var castedValue = (TimeSpan) value;
            return new DateTime(DateTime.MinValue.Year, DateTime.MinValue.Month, DateTime.MinValue.Day, castedValue.Hours,
                castedValue.Minutes, castedValue.Seconds);
        }
    }
}