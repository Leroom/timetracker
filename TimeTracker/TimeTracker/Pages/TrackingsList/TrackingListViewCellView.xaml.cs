﻿using System;
using Xamarin.Forms;

namespace TimeTracker.Pages.TrackingsList
{
    public partial class TrackingListViewCell : ViewCell
    {
        public TrackingListViewCell()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty ActivityNameProperty =
            BindableProperty.Create(nameof(ActivityName), typeof(string), typeof(TrackingListViewCell), "");

        public static readonly BindableProperty ActivityCategoryProperty =
            BindableProperty.Create(nameof(ActivityCategory), typeof(string), typeof(TrackingListViewCell), "");

        public static readonly BindableProperty EndTimeProperty =
            BindableProperty.Create(nameof(EndTime), typeof(DateTime), typeof(TrackingListViewCell), new DateTime());

        public static readonly BindableProperty StartTimeProperty =
            BindableProperty.Create(nameof(StartTime), typeof(DateTime), typeof(TrackingListViewCell), new DateTime());

        public static readonly BindableProperty DurationProperty =
            BindableProperty.Create(nameof(Duration), typeof(TimeSpan), typeof(TrackingListViewCell), new TimeSpan());

        public string ActivityName
        {
            get { return (string)GetValue(ActivityNameProperty); }
            set { SetValue(ActivityNameProperty, value); }
        }

        public string ActivityCategory
        {
            get { return (string)GetValue(ActivityCategoryProperty); }
            set { SetValue(ActivityCategoryProperty, value); }
        }

        public DateTime EndTime
        {
            get { return (DateTime)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }

        public DateTime StartTime
        {
            get { return (DateTime)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        public TimeSpan Duration
        {
            get { return (TimeSpan)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }
    }
}