﻿using System.Collections.ObjectModel;
using TimeTracker.Models;

namespace TimeTracker.Pages.TrackingsList
{
    public class TrackingsListViewModel
    {
        public TrackingsListViewModel()
        {
            TrackingsList = new ObservableCollection<TrackingModel>();
        }

        public ObservableCollection<TrackingModel> TrackingsList { get; set; }

        public void Load()
        {
            var trackingsList = App.Database.GetAllOrdered(model => model.StartTime, false).Result;

            TrackingsList.Clear();
            foreach (var trackingModel in trackingsList)
                TrackingsList.Add(trackingModel);
        }
    }
}