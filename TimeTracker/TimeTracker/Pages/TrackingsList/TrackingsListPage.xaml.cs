﻿using System;
using TimeTracker.Models;
using TimeTracker.Pages.TrackingEditor;
using Xamarin.Forms;

namespace TimeTracker.Pages.TrackingsList
{
    public partial class TrackingsListPage : ContentPage
    {
        public TrackingsListPage()
        {
            ViewModel = new TrackingsListViewModel();
            InitializeComponent();
        }

        public TrackingsListViewModel ViewModel { get; set; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.Load();
        }

        private void OnAddClicked(object sender, EventArgs e)
        {
            var editorPage = new TrackingEditorPage(null);
            Navigation.PushAsync(editorPage);
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var tappedItemId = ((TrackingModel) e.Item).Id;
            var editorPage = new TrackingEditorPage(tappedItemId);
            Navigation.PushAsync(editorPage);
        }
    }
}