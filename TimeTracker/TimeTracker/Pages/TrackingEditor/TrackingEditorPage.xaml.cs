﻿using Xamarin.Forms;

namespace TimeTracker.Pages.TrackingEditor
{
    public partial class TrackingEditorPage : ContentPage
    {
        public TrackingEditorPage(string trackingId)
        {
            ViewModel = new TrackingEditorViewModel(trackingId, Navigation);
            InitializeComponent();
        }

        public TrackingEditorViewModel ViewModel { get; set; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.Load();
        }
    }
}