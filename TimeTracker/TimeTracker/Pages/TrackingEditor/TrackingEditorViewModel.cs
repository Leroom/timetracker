﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeTracker.Models;
using Xamarin.Forms;

namespace TimeTracker.Pages.TrackingEditor
{
    public class TrackingEditorViewModel : INotifyPropertyChanged
    {
        private readonly INavigation _navigation;

        public TrackingEditorViewModel(INavigation navigation)
        {
            _navigation = navigation;
            SaveCommand = new Command(async() => await Save());
        }

        private bool CanExecute()
        {
            return string.IsNullOrEmpty(this.ActivityName) == false &&
                   string.IsNullOrEmpty(this.ActivityCategory) == false &&
                   Duration.TotalSeconds > 0;
        }

        public TrackingEditorViewModel(string id, INavigation navigation) : this(navigation)
        {
            Id = id;
            _trackingDate = DateTime.UtcNow.Date;
        }

        public void Load()
        {
            if (Id == null) return;

            var tracking = App.Database.GetAsync(Id).Result;
            ConvertToViewModel(tracking);
        }


        #region Properties
        private string _activityName;

        public string ActivityName
        {
            get { return _activityName; }
            set
            {
                if (_activityName == value) return;

                _activityName = value;
                OnPropertyChanged();
            }
        }

        private string _activityCategory;

        public string ActivityCategory
        {
            get { return _activityCategory; }
            set
            {
                if (_activityCategory == value) return;

                _activityCategory = value;
                OnPropertyChanged();
            }
        }

        private DateTime _trackingDate { get; set; }

        private TimeSpan _startTime;

        public TimeSpan StartTime
        {
            get { return _startTime; }
            set
            {
                if (_startTime == value) return;

                _startTime = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Duration));
            }
        }

        private TimeSpan _endTime;

        public TimeSpan EndTime
        {
            get { return _endTime; }
            set
            {
                if (_endTime == value) return;

                _endTime = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Duration));
            }
        }

        public string Id { get; private set; }

        public TimeSpan Duration => new TimeSpan(EndTime.Subtract(StartTime).Ticks);
        #endregion

        #region INPCImplementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public ICommand SaveCommand { get; }

        public async Task Save()
        {
            var trackingModel = ConvertToModel(this);
            await App.Database.Save(trackingModel);
            await _navigation.PopAsync();
        }

        #region Converters
        private void ConvertToViewModel(TrackingModel tracking)
        {
            this.ActivityName = tracking.ActivityName;
            this.ActivityCategory = tracking.ActivityCategory;
            this._trackingDate = tracking.StartTime.Date;
            this.StartTime = tracking.StartTime.TimeOfDay;
            this.EndTime = tracking.EndTime.TimeOfDay;
        }

        private TrackingModel ConvertToModel(TrackingEditorViewModel trackingEditorViewModel)
        {
            return new TrackingModel
            {
                Id = trackingEditorViewModel.Id,
                ActivityCategory = trackingEditorViewModel.ActivityCategory,
                ActivityName = trackingEditorViewModel.ActivityName,
                StartTime = _trackingDate.AddTicks(trackingEditorViewModel.StartTime.Ticks),
                EndTime = _trackingDate.AddTicks(trackingEditorViewModel.EndTime.Ticks)
            };
        } 
        #endregion
    }
}