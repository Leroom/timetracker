﻿using System;
using SQLite.Net.Attributes;

namespace TimeTracker.Models
{
    public class TrackingModel
    {
	    [PrimaryKey]
        public string Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public TimeSpan Duration => new TimeSpan(EndTime.Subtract(StartTime).Ticks);

        public string ActivityName { get; set; }

        public string ActivityCategory { get; set; }
    }
}