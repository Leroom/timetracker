﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SQLite.Net;
using SQLite.Net.Async;
using TimeTracker.Models;

namespace TimeTracker
{
    public class TrackingsDatabase
    {
        public TrackingsDatabase(ILocalFilePathProvider localFilePathProvider,
            ISqlitePlatformProvider sqlitePlatformProvider)
        {
            var pathToDatabaseFile = localFilePathProvider.GetLocalPathToFile("Database.db3");
            var platform = sqlitePlatformProvider.GetPlatform();

            _connection = new SQLiteAsyncConnection(() =>
                new SQLiteConnectionWithLock(platform, new SQLiteConnectionString(pathToDatabaseFile, true)));
            _connection.CreateTableAsync<TrackingModel>().Wait();
        }

        private readonly SQLiteAsyncConnection _connection;

        public Task Save(TrackingModel trackingModel)
        {
            if (trackingModel.Id == null)
            {
                trackingModel.Id = Guid.NewGuid().ToString();
            }

            return _connection.RunInTransactionAsync((SQLiteConnection connection) =>
            {
                connection.InsertOrReplace(trackingModel);
            });
        }

        public Task<TrackingModel> GetAsync(string id)
        {
            return _connection.GetAsync<TrackingModel>(m => m.Id == id);
        }

        public Task<List<TrackingModel>> GetAll()
        {
            return _connection.Table<TrackingModel>().ToListAsync();
        }

        public Task<List<TrackingModel>> GetAllOrdered<TOrderingValue>(
            Expression<Func<TrackingModel, TOrderingValue>> orderByClause, bool orderAscending = true)
        {
            if (orderByClause == null)
                return GetAll();

            var baseQuery = _connection.Table<TrackingModel>();
            
            if (orderAscending)
                baseQuery.OrderBy(orderByClause);
            else
                baseQuery.OrderByDescending(orderByClause);

            return baseQuery.ToListAsync();
        }
    }
}