﻿using SQLite.Net.Interop;

namespace TimeTracker
{
    public interface ISqlitePlatformProvider
    {
        ISQLitePlatform GetPlatform();
    }
}