﻿using Autofac;

namespace TimeTracker.Droid
{
    public class PlatformSpecificModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<LocalFilePathProvider>().As<ILocalFilePathProvider>();
            builder.RegisterType<SqlitePlatformProvider>().As<ISqlitePlatformProvider>();
        }
    }
}