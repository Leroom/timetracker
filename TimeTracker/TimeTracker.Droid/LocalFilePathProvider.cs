﻿using System;
using System.IO;
using TimeTracker.Droid;
using Xamarin.Forms;
[assembly: Dependency(typeof(LocalFilePathProvider))]
namespace TimeTracker.Droid
{
    public class LocalFilePathProvider : ILocalFilePathProvider
    {
        public string GetLocalPathToFile(string fileName)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), fileName);
        }
    }
}
