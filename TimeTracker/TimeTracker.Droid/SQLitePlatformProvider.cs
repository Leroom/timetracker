﻿using SQLite.Net.Interop;
using SQLite.Net.Platform.XamarinAndroid;
using TimeTracker.Droid;
using Xamarin.Forms;
[assembly: Dependency(typeof(SqlitePlatformProvider))]
namespace TimeTracker.Droid
{
    public class SqlitePlatformProvider : ISqlitePlatformProvider
    {
        public ISQLitePlatform GetPlatform()
        {
            return new SQLitePlatformAndroid();
        }
    }
}
