﻿using SQLite.Net.Interop;
using SQLite.Net.Platform.XamarinIOS;
using TimeTracker.iOS;
using Xamarin.Forms;
[assembly: Dependency(typeof(SqlitePlatformProvider))]
namespace TimeTracker.iOS
{
    public class SqlitePlatformProvider : ISqlitePlatformProvider
    {
        public ISQLitePlatform GetPlatform()
        {
            return new SQLitePlatformIOS();
        }
    }
}
