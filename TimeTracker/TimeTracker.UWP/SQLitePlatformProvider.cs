﻿using SQLite.Net.Interop;
using SQLite.Net.Platform.WinRT;
using TimeTracker.UWP;
using Xamarin.Forms;
[assembly: Dependency(typeof(SqlitePlatformProvider))]
namespace TimeTracker.UWP
{
    public class SqlitePlatformProvider : ISqlitePlatformProvider
    {
        public ISQLitePlatform GetPlatform()
        {
            return new SQLitePlatformWinRT();
        }
    }
}
