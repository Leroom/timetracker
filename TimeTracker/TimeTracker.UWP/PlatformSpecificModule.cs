﻿using Autofac;

namespace TimeTracker.UWP
{
    public class PlatformSpecificModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<LocalFilePathProvider>().As<ILocalFilePathProvider>();
            builder.RegisterType<SqlitePlatformProvider>().As<ISqlitePlatformProvider>();
        }
    }
}