﻿using System.IO;
using Windows.Storage;
using TimeTracker.UWP;
using Xamarin.Forms;
[assembly: Dependency(typeof(LocalFilePathProvider))]
namespace TimeTracker.UWP
{
    public class LocalFilePathProvider : ILocalFilePathProvider
    {
        public string GetLocalPathToFile(string fileName)
        {
            return Path.Combine(ApplicationData.Current.LocalFolder.Path, fileName);
        }
    }
}